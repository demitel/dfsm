#include <stdio.h>
#include "sample.h"

char *name(int state)
{
	char *n;
	switch (state) {
		case 0x00:
			n = "stateInitialisation";
			break;
		case 0x04:
			n = "stateStopped";
			break;
		case 0x05:
			n = "stateOperational";
			break;
		case 0x7F:
			n = "statePreOp";
			break;
	}
	return n;
}

extern co_states_num_t co_state;

int queue[] = {cmdInitialising, cmdReset, cmdResetComm, cmdPreOp,
	cmdStartOp, cmdStop, cmdPreOp,
	cmdStop, cmdStartOp, cmdPreOp,
	cmdResetComm,
};

int main()
{
	int i, n, t;

	n = sizeof(queue)/sizeof(queue[0]);

	printf("Initial state: 0x%.2x (%d): %s\n", co_getState(), co_state, name(co_getState()));
	for (i = 0; i < n; i++) {
		t = co_transitState(queue[i]);
		printf("transit: %.2d, state: 0x%.2x (%d): %s\n",
			t, co_getState(), co_state, name(co_getState()));
	}

	return 0;
}
