Simple finite state machine generator in perl.
It generates .c and .h file for usage in your project.

Usage: dfsm.pl <input file>

(c) Denis Sirotkin <denis@demitel.ru>

You can use this code as you want without any restrictions. Thanks!
